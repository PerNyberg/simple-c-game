#include <iostream>
#include <stdio.h>
#include "SDL.h"
#include "SDL_image.h"
#include "MovingSprite.h"


/*
 * Class to represent moving MovingSprites in the game. Used by the GameEngine class to represent bullets, moving enemies possibly player
 * Contains SDL_Texture and SDL_Rect for the MovingSprite
 */


MovingSprite::MovingSprite(int x, int y, int w, int h, std::string fn, int xS, int yS,bool hs):Sprite(x,y,w,h,fn),xSpeed(xS),ySpeed(yS),hostile(hs)
{
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
}

MovingSprite* MovingSprite::getInstance(int x, int y, int w, int h, std::string fn, int xS, int yS,bool hs){
	return new MovingSprite(x,y,w,h,fn,xS,yS,hs);
}

void MovingSprite::tick(SDL_Renderer* render)		// function used by the GameEngine class to render the MovingSprite every frame in the game loop
{
	texture = IMG_LoadTexture(render,fileName.c_str());
	rect.x = rect.x+xSpeed;
	rect.y = rect.y+ySpeed;
	SDL_RenderCopy(render,texture,NULL,&rect);
}

bool MovingSprite::getHostile()
{
	return hostile;
}

MovingSprite::~MovingSprite()
{
	SDL_DestroyTexture(texture);
}
