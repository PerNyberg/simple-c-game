#ifndef MOVINGSPRITE_H
#define MOVINGSPRITE_H

#include <string>
#include <vector>
#include "SDL.h"
#include "Sprite.h"

class MovingSprite : public Sprite
{

public:
	static MovingSprite* getInstance(int x, int y, int w, int h, std::string fn, int xS, int yS,bool hs);		// fabriksfunktion
	void tick(SDL_Renderer* render);
	bool getHostile();
	~MovingSprite();
private:
	MovingSprite(int x, int y, int w, int h, std::string fn, int xS, int yS,bool hs);
	int xSpeed;
	int ySpeed;
	bool hostile;

};



#endif
