#include "SDL.h"
#include "SDL_image.h"
#include "Sprite.h"

/*
 * Class to represent moving sprites in the game. Used by the GameEngine class.
 * Contains SDL_Texture and SDL_Rect for the sprite
 */

Sprite::Sprite(int x, int y, int w, int h, std::string fn) : fileName(fn)
{
}

int Sprite::getX()
{
	return rect.x;
}
int Sprite::getY()
{
	return rect.y;
}

SDL_Rect Sprite::getRect()
{
	return rect;
}

void Sprite::setX(int nx)
{
	rect.x = nx;
}
void Sprite::setY(int ny)
{
	rect.y = ny;
}

Sprite::~Sprite()
{
}
