#ifndef SCORETEXT_H
#define SCORETEXT_H

#include <stdio.h>
#include <sstream>
#include <string>
#include "SDL.h"
#include "SDL_ttf.h"
#include "Text.h"

class ScoreText : public Text
{

public:
	ScoreText(std::string fontPath,int x, int y);
	void tick(SDL_Renderer* render);
	void addScore();
	int getScore();
	~ScoreText();
private:
	int score;		// the current score
	std::ostringstream convert;		// stream used to convert int to string

};


#endif
