#include <iostream>
#include <stdio.h>
#include "SDL.h"
#include "StaticText.h"

StaticText::StaticText(std::string fontPath,std::string textToDisplay,int x, int y, int textSize):Text(fontPath,x,y)
{
	if(TTF_Init() == -1)				// Init TTF
	{
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
	}
	font = TTF_OpenFont(fontPath.c_str(),textSize);
	if(font == NULL )
	{
		printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	textRect.x = x;
	textRect.y = y;
	color = {255,250,250,255};
	text = textToDisplay;
}

void StaticText::tick(SDL_Renderer* render)
{
	textSurface = TTF_RenderText_Solid(font,text.c_str(),color);
	textTexture = SDL_CreateTextureFromSurface(render, textSurface);
	SDL_QueryTexture(textTexture,NULL,NULL,&textRect.w,&textRect.h);
	SDL_RenderCopy(render,textTexture,NULL,&textRect);
}


StaticText::~StaticText()
{
	TTF_CloseFont(font);
	delete textSurface;
	SDL_DestroyTexture(textTexture);
	TTF_Quit();
}
