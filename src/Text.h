#ifndef Text_H
#define Text_H

#include <sstream>
#include <string>
#include "SDL.h"
#include "SDL_ttf.h"

/*
 * 	Superclass to ScoreText and StaticText
 */

class Text
{

public:
	virtual void tick(SDL_Renderer* render)=0;
	virtual ~Text();
protected:
	Text(std::string fontPath,int x, int y);
	std::string text;
	TTF_Font* font;
	SDL_Color color;
	SDL_Surface* textSurface;
	SDL_Texture* textTexture;
	SDL_Rect textRect;

	Text(const Text& other); // copy konstruktor
	const Text& operator=(const Text& other); // tildelning

};


#endif
