#ifndef STATICSPRITE_H
#define STATICSPRITE_H

#include <string>
#include "SDL.h"
#include "Sprite.h"

class StaticSprite : public Sprite
{

public:
	static StaticSprite* getInstance(int x, int y, int w, int h, std::string fn);		// fabriksfunktion
	void tick(SDL_Renderer* render);
	~StaticSprite();
private:
	StaticSprite(int x, int y, int w, int h, std::string fn);
};



#endif
