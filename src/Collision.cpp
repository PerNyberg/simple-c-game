#include "SDL.h"
#include "Collision.h"

/*
 * Class used by GameEngine class to check if two SDL_Rect have collided.
 */

Collision::Collision()
{
}

bool Collision::checkCollision(const SDL_Rect &rect1, const SDL_Rect &rect2)
{

	int left1 = rect1.x;		// Find edges of rect1
	int right1 = rect1.x + rect1.w;
	int top1 = rect1.y;
	int bottom1 = rect1.y + rect1.h;

	int left2 = rect2.x;		// Find edges of rect2
	int right2 = rect2.x + rect2.w;
	int top2 = rect2.y;
	int bottom2 = rect2.y + rect2.h;

	// Check edges
	if ( left1 > right2 )// Left 1 is right of right 2
		return false; // No collision
	if ( right1 < left2 ) // Right 1 is left of left 2
		return false; // No collision
	if ( top1 > bottom2 ) // Top 1 is below bottom 2
		return false; // No collision
	if ( bottom1 < top2 ) // Bottom 1 is above top 2
		return false; // No collision

	return true;
}

Collision::~Collision()
{
}
