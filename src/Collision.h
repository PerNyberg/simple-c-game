#ifndef COLLISION_H
#define COLLISION_H

#include "SDL.h"

class Collision
{
	public:
		Collision();
		bool checkCollision( const SDL_Rect &rect1, const SDL_Rect &rect2);
		~Collision();
};


#endif

