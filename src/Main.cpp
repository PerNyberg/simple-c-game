#include <iostream>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include "Sprite.h"
#include "ScoreText.h"
#include "GameEngine.h"


int main(int argc, char *argv[]){

	GameEngine* ge = new GameEngine("Space Game","graphics\\spacebackground.bmp",600,750,400,200,60);		// creating gameengine object

	std::vector<std::string> animationImagePaths		// vector with images of the players sprite, enables animation
	{
	"graphics\\animatedship\\ship1.png",
	"graphics\\animatedship\\ship2.png",
	"graphics\\animatedship\\ship3.png",
	"graphics\\animatedship\\ship4.png",
	"graphics\\animatedship\\ship5.png",
	"graphics\\animatedship\\ship6.png",
	"graphics\\animatedship\\ship7.png",
	"graphics\\animatedship\\ship8.png",
	"graphics\\animatedship\\ship9.png",
	"graphics\\animatedship\\ship10.png",
	"graphics\\animatedship\\ship11.png"
	};

	AnimatedSprite* anim_spaceship = AnimatedSprite::getInstance(180,350,60,120,"",animationImagePaths,0,0,10);

	ge->addOncomingEnemy("graphics\\asteroid.png",false,25);		// add enemy
	ge->addOncomingEnemy("graphics\\enemyship1.png",true,50);
	ge->addOncomingEnemy("graphics\\enemyship2.png",true,50);
	ge->addOncomingEnemy("graphics\\enemyship3.png",true,50);
	ge->addAnimatedPlayer(anim_spaceship);
	ge->addScoreText("consola.ttf",0,0);
	ge->addStaticSprite("graphics\\planet1.png",50,100,50,50);
	ge->addStaticSprite("graphics\\planet2.png",400,450,50,50);
	ge->setGameOverTextXY(140, 300);			// set the size of the text that is shown when the player dies


	ge->run();		// game engine starts the game loop

	delete ge;
	return 0;
}
