#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include "SDL.h"
#include "SDL_image.h"
#include "GameEngine.h"
#include "Collision.h"


/*
 * 	Class to act as a game engine for a space invaders type game.
 * 	The engine will accept parameters as background and size of the game window through the constructor.
 * 	The implementation will then use the method addPlayer or addAnimatedPlayer to add a sprite to be used as the players space ship.
 * 	Lastly the function run starts the game loop.
 *
 * 	Help-functions as checkCollision and tickComponents are used in the game loop.
 * 	The function "createRandomEnemies" uses the defined enemies in the enemyDefinitions-vector containing instances of the Enemy-struct to create the oncoming enemies that the player should kill.
 */

GameEngine::GameEngine(std::string title,std::string bgPath,int w, int h,int winXPos, int winYPos, int fps) :backgroundPath(bgPath),windowXPos(winXPos),windowYPos(winYPos),enemyFrequency(300),enemySpeed(1),fpsInMs(1000/fps),quit(false)	// constructor
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0){			// INIT SDL
		printf("SDL Init error: %s\n", SDL_GetError());
	}
    window = SDL_CreateWindow(title.c_str(), winXPos, winYPos, w, h, SDL_WINDOW_SHOWN);				// create game window
    if(window == NULL)
    {
    	printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
    }
    render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);	// create renderer
    if(render == NULL)
    {
    	printf("Render could not be created! SDL_Error: %s\n", SDL_GetError());
    }
    background = IMG_LoadTexture(render,bgPath.c_str());		// load the background image as a texture to be used in the game window
	backgroundRect.x = 0;
	backgroundRect.y = 0;
	backgroundRect.w = w;
	backgroundRect.h = h;
	coll = new Collision();				// create the Collision object to check for collisions
}

void GameEngine::addPlayer(MovingSprite* spr)	// add the sprite to be used for the players space ship
{
	player = spr;
}

void GameEngine::addAnimatedPlayer(AnimatedSprite* spr)	// add the sprite to be used for the players space ship, use this if player should be animated
{
	player = spr;
}

void GameEngine::addOncomingEnemy(std::string fn,bool h,int s)		// create instance of struct Enemy that is used when random enemies are spawned add the texture path, size and bool that decides if the enemies should shoot or not
{
	Enemy e(h,fn,s);
	enemyDefinitions.push_back(e);
}

void GameEngine::addStaticSprite(std::string sp,int x, int y,int w, int h)		// add static sprite
{
	StaticSprite* staticSprite = StaticSprite::getInstance(x,y,w,h,sp);
	artObjects.push_back(staticSprite);
}

void GameEngine::addScoreText(std::string fontPath,int x, int y)		// add text that shows score
{
	scoreText = new ScoreText(fontPath,x,y);
}

void GameEngine::setGameOverTextXY(int x, int y)			// set the size of the text that is shown when the player dies
{
	gameOverTextX = x;
	gameOverTextY = y;
}

void GameEngine::setEnemyFrequency(int frequency)		// sets the frequency of the spawning enemies, every frame there is a chance in between 1 and "frequency" of an enemy spawning
{
	enemyFrequency = frequency;
}

void GameEngine::setEnemySpeed(int speed)		// set the speed of enemies
{
	enemySpeed = speed;
}

void GameEngine::createRandomEnemies()			// randomly creating oncoming enemies
{
	if((rand() % enemyFrequency + 1) > enemyFrequency/2)			// only sometimes when this function is called it will actually create a new enemy
	{
		for(Enemy e : enemyDefinitions)			// iterate through the vector containing the definitions for the enemies
		{
			if((rand() % enemyFrequency + 1) == 1)
			{
				std::cout << e.hostile << std::endl;
				MovingSprite* enemy = MovingSprite::getInstance((rand()%((backgroundRect.w-70)-25))+30,-30,e.size,e.size,e.fileName,0,enemySpeed,e.hostile);
				enemies.push_back(enemy);
			}
		}
	}
}

void GameEngine::gameOver()		// prints out game over and score, pauses game, lets user close window
{
	StaticText* st = new StaticText("consola.ttf","GAME OVER",gameOverTextX+50,gameOverTextY,40);
	StaticText* st2 = new StaticText("consola.ttf","press ESC to quit",gameOverTextX,gameOverTextY+55,30);
	st->tick(render);
	st2->tick(render);
	SDL_RenderPresent(render);
	while(quit == false)
	{
		while(SDL_PollEvent(&event))	// Getting next event in the event que
		{
			switch (event.type) {
				case SDL_KEYDOWN:
					if (event.key.keysym.sym == SDLK_ESCAPE)
						quit = true;
					break;
				case SDL_QUIT:
					quit = true;
					break;
			}
		}
	}
}

void GameEngine::checkCollision()			// checking collisions between player and components
{
	for(int i = 0;i<bullets.size();i++){	// going through bullets and enemies to check for collisions between them
		if((coll->checkCollision(bullets[i]->getRect(),player->getRect())) == true)
			gameOver();
		for(int p = 0;p<enemies.size();p++){
			if((coll->checkCollision(bullets[i]->getRect(),enemies[p]->getRect())) == true){	// if this is true, then the involved objects are removed
				scoreText->addScore();			// add score
				delete bullets[i];
				bullets.erase(bullets.begin() + i);
				delete enemies[p];
				enemies.erase(enemies.begin() + p);
			}
		}
	}
	for(int p = 0;p<enemies.size();p++){	// going through the oncoming enemies and checking if there is a collision with the player
		if(coll->checkCollision(player->getRect(),enemies[p]->getRect()) == true)
		{
			gameOver();
		}
	}
}

void GameEngine::tickComponents()		// iterating through bullets, enemies and invoking the function "tick" in order to render their textures and moving x and y positions
{
	for(int i = 0;i<artObjects.size();i++)		// tick artObjects, or static sprites
	{
			artObjects[i]->tick(render);
	}
	player->tick(render);		// tick player
	for(int i = 0;i<bullets.size();i++)			// deleting bullets that are no longer on the screen, or ticking them if they are
	{
		if(bullets[i]->getY() > backgroundRect.w+250 || bullets[i]->getY() < -100)
		{
			delete bullets[i];
			bullets.erase(bullets.begin() + i);
		}
		else
		{
			bullets[i]->tick(render);
		}
	}
	for(int i = 0;i<enemies.size();i++)
	{
		if((rand() % 250 + 1) == 1 && enemies[i]->getHostile() != false)		// make the enemies shoot bullets randomly
		{
			MovingSprite* bullet = MovingSprite::getInstance(enemies[i]->getX()+enemies[i]->getRect().w/2,enemies[i]->getY()+80,3,12,"graphics\\bullet.png",0,2,false);
			bullets.push_back(bullet);
		}

		if(enemies[i]->getY() > backgroundRect.w+250 || enemies[i]->getY() < -100)		// checking if enemy is on the screen or not, deleting if its not
		{
			delete enemies[i];
			enemies.erase(enemies.begin() + i);
		}
		else
		{
			enemies[i]->tick(render);
		}
	}
	scoreText->tick(render);
}

void GameEngine::run()				// function that contains the game loop, using help functions such as createRandomEnemies() and tickComponents().
{
	while(quit == false)		// GAME LOOP
	{
		frameStart = SDL_GetTicks();
		createRandomEnemies();
		SDL_RenderClear(render);
		SDL_RenderCopy(render,background,NULL,&backgroundRect);
		tickComponents();
		SDL_RenderPresent(render);
		checkCollision();				// checking for collisions using the checkCollisionsfunction
		while(SDL_PollEvent(&event))	// Getting next event in the event que
		{
			switch (event.type) {
				case SDL_KEYDOWN:
					if (event.key.keysym.sym == SDLK_ESCAPE)
						quit = true;
					if (event.key.keysym.sym == SDLK_UP || event.key.keysym.sym == SDLK_w)
						if(player->getY() > 10)
							player->setY(player->getY()-15);
					if (event.key.keysym.sym == SDLK_DOWN || event.key.keysym.sym == SDLK_s)
						if(player->getY() < backgroundRect.h-100)
							player->setY(player->getY()+15);
					if (event.key.keysym.sym == SDLK_LEFT || event.key.keysym.sym == SDLK_a)
						if(player->getX() > -10)
							player->setX(player->getX()-15);
					if (event.key.keysym.sym == SDLK_RIGHT || event.key.keysym.sym == SDLK_d)
						if(player->getX() < backgroundRect.w-90)
							player->setX(player->getX()+15);
					if (event.key.keysym.sym == SDLK_SPACE){
						MovingSprite* bullet = MovingSprite::getInstance(player->getX()+30,player->getY()-10,3,12,"graphics\\bullet.png",0,-5,false);
						bullets.push_back(bullet);
					}
					break;
				case SDL_QUIT:
					quit = true;
					break;
			}
		}
		frameEnd = SDL_GetTicks();
		elapsed = (frameEnd-frameStart);
		if(elapsed<fpsInMs){
			SDL_Delay(fpsInMs-elapsed);
		}
	}
}

GameEngine::~GameEngine()			// deleting dynamically allocated memory
{
    for (int i=0;i<bullets.size();i++)	// TAR BORT ALLT I COMPONENTS
    {
        delete bullets[i];
    }
    bullets.clear();
    for (int i=0;i<enemies.size();i++)	// TAR BORT ALLT I COMPONENTS
    {
        delete enemies[i];
    }
    enemies.clear();

    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(render);
    SDL_DestroyTexture(background);
    delete player;
    delete coll;
}
