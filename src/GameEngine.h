#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <vector>
#include <string>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "Sprite.h"
#include "MovingSprite.h"
#include "StaticSprite.h"
#include "AnimatedSprite.h"
#include "Collision.h"
#include "ScoreText.h"
#include "StaticText.h"
#include "Text.h"

class GameEngine
{

public:
	GameEngine(std::string title,std::string bgPath,int w, int h,int winXPos, int winYPos, int fps);
	void run();
	void addPlayer(MovingSprite* spr);
	void addAnimatedPlayer(AnimatedSprite* spr);
	void addOncomingEnemy(std::string ep,bool hostile,int s);
	void addStaticSprite(std::string sp,int x, int y,int w, int h);
	void addScoreText(std::string fontPath,int x, int y);
	void createRandomEnemies();
	void tickComponents();
	void checkCollision();
	void setGameOverTextXY(int x, int y);
	void setEnemyFrequency(int frequency);
	void setEnemySpeed(int speed);
	void gameOver();
	~GameEngine();

	struct Enemy{		// struct used to define enemies that should have a random chance of spawning, used by the "createRandomEnemies" function
	            bool hostile;
	            std::string fileName;
	            int size;
	            Enemy(bool h,std::string fn,int s){hostile = h,fileName = fn,size = s;}
	};

private:
	SDL_Window* window;
	SDL_Renderer* render;
	SDL_Texture* background;
	SDL_Rect backgroundRect;
	std::vector<MovingSprite*> bullets;
	std::vector<MovingSprite*> enemies;
	std::vector<StaticSprite*> artObjects;
	std::string backgroundPath;
	std::vector<Enemy> enemyDefinitions;
	ScoreText* scoreText;
	SDL_Event event;
	Uint32 fpsInMs;
	Uint32 frameStart;
	Uint32 frameEnd;
	int elapsed;

	bool quit;
	Sprite* player;
	Collision* coll;
	int windowXPos;
	int windowYPos;
	int gameOverTextX;
	int gameOverTextY;
	int enemyFrequency;
	int enemySpeed;
};


#endif
