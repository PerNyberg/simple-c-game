#ifndef STATICTEXT_H
#define STATICTEXT_H

#include <sstream>
#include <string>
#include "SDL.h"
#include "SDL_ttf.h"
#include "Text.h"

class StaticText : public Text
{

public:
	StaticText(std::string fontPath,std::string textToDisplay,int x, int y,int textSize);
	void tick(SDL_Renderer* render);
	~StaticText();
private:
};


#endif
