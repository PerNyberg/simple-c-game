#include "SDL.h"
#include "ScoreText.h"

/*
	Sub class of Text.
	Is used to represent the text showing the current score.
 */

ScoreText::ScoreText(std::string fontPath,int x, int y):score(0),Text(fontPath,x,y)
{
	if(TTF_Init() == -1)		// INIT TTF
	{
		printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	text = "Score: 0";
	font = TTF_OpenFont(fontPath.c_str(),20);
	if(font == NULL )
	{
		printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	textRect.x = x;
	textRect.y = y;
	color = {255,250,250,255};
}

void ScoreText::tick(SDL_Renderer* render)
{
	SDL_QueryTexture(textTexture,NULL,NULL,&textRect.w,&textRect.h);
	textSurface = TTF_RenderText_Solid(font,text.c_str(),color);
	textTexture = SDL_CreateTextureFromSurface(render, textSurface);
	SDL_RenderCopy(render,textTexture,NULL,&textRect);
}

void ScoreText::addScore()		// when addScore is called, int score is increased and a stream is used to convert the score to a string
{
	score++;
	convert.str("");
	convert << score;
	text = "Score: "+convert.str();
}

int ScoreText::getScore()
{
	return score;
}

ScoreText::~ScoreText()
{
	TTF_CloseFont(font);
	delete textSurface;
	SDL_DestroyTexture(textTexture);
	TTF_Quit();
}
