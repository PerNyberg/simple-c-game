#ifndef SPRITE_H
#define SPRITE_H

#include <string>
#include "SDL.h"

class Sprite
{

public:
	virtual void tick(SDL_Renderer* render)=0;
	virtual ~Sprite();
	int getX();
	int getY();
	SDL_Rect getRect();
	void setX(int nx);
	void setY(int ny);
protected:
	std::string fileName;		// fileName to the image that should be drawn
	SDL_Rect rect;				// the sprites SDL_Rect
	SDL_Texture* texture;		// the texture that is drawn

	Sprite(int x, int y, int w, int h, std::string fn);

	Sprite(const Sprite& other);		// copy constructor
	const Sprite& operator=(const Sprite& other);

};



#endif
