#include <iostream>
#include "SDL.h"
#include "SDL_image.h"
#include "AnimatedSprite.h"
#include <stdio.h>

/*
 * Class to represent an AnimatedSprite in the game. Used by the GameEngine class to create the player sprite as an animated object
 */


AnimatedSprite::AnimatedSprite(int x, int y, int w, int h, std::string fn,std::vector<std::string> ip, int xS, int yS,int as):Sprite(x,y,w,h,fn),xSpeed(xS),ySpeed(yS),currentImage(0),imagePaths(ip),countUp(true),animationSpeed(10)
{
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
}

AnimatedSprite* AnimatedSprite::getInstance(int x, int y, int w, int h, std::string fn,std::vector<std::string> ip, int xS, int yS,int as){		// fabriksfunktion
	return new AnimatedSprite(x,y,w,h,fn,ip,xS,yS,as);
}

void AnimatedSprite::tick(SDL_Renderer* render)		// function used by the GameEngine class to render the AnimatedSprite every frame in the game loop, contains logic that changes the current image to be shown
{
	texture = IMG_LoadTexture(render,imagePaths[currentImage].c_str());
	SDL_RenderCopy(render,texture,NULL,&rect);

	if((rand() % animationSpeed + 1) == animationSpeed){
		if(countUp){
			currentImage++;
		}else{
			currentImage--;
		}
		if(currentImage == imagePaths.size()-1){
			countUp = false;
		}else if(currentImage == 0){
			countUp = true;
		}
	}
}

AnimatedSprite::~AnimatedSprite()
{
	SDL_DestroyTexture(texture);
}
