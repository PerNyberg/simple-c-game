#ifndef ANIMATEDSPRITE_H
#define ANIMATEDSPRITE_H

#include <string>
#include <vector>
#include "SDL.h"
#include "Sprite.h"

class AnimatedSprite : public Sprite
{

public:
	static AnimatedSprite* getInstance(int x, int y, int w, int h, std::string fn,std::vector<std::string> ip, int xS, int yS,int as);		// fabriksfunktion
	void tick(SDL_Renderer* render);
	~AnimatedSprite();
private:
	AnimatedSprite(int x, int y, int w, int h, std::string fn,std::vector<std::string> ip, int xS, int yS,int as);
	int xSpeed;
	int ySpeed;
	std::vector<std::string> imagePaths;		// vector containing the images of the animation
	int currentImage;		// int that points out the current image part of the animation in the imagePaths vector
	bool countUp;			// bool used to make the tick function cycle between the images correctly
	int animationSpeed;		// how fast the animation will run, if set to four, the tick will only update the animation once every 4 ticks

};



#endif
