#include "SDL.h"
#include "SDL_image.h"
#include "StaticSprite.h"
#include <iostream>

/*
 * Class to represent moving StaticSprites in the game. Used by the GameEngine class.
 * Contains SDL_Texture and SDL_Rect for the StaticSprite
 */


StaticSprite::StaticSprite(int x, int y, int w, int h, std::string fn):Sprite(x,y,w,h,fn)
{
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
}

StaticSprite* StaticSprite::getInstance(int x, int y, int w, int h, std::string fn){
	return new StaticSprite(x,y,w,h,fn);
}

void StaticSprite::tick(SDL_Renderer* render)		// function used by the GameEngine class to render the StaticSprite every frame in the game loop
{
	texture = IMG_LoadTexture(render,fileName.c_str());
	SDL_RenderCopy(render,texture,NULL,&rect);
}

StaticSprite::~StaticSprite()
{
	SDL_DestroyTexture(texture);
}

